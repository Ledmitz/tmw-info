#!/bin/bash
# TMW-info
(C) Ledmitz, 2020 - GPL 3.0

## Related
* TMW GM Log Downloader (original): https://gitlab.com/liviorecchia/tmw-gm-log-downloader
* TMW GM Log Downloader(deprecated): https://gitlab.com/Ledmitz/tmw-gm-log-downloader
* tmw-gmlog(deprecated): https://gitlab.com/Ledmitz/tmw-gmlog
* tmw-info: https://git.themanaworld.org/legacy/tmw-info/
* The Mana World: https://themanaworld.org]

## Dependencies
Requires: Linux, Bourne Again Shell (BASH), grep, curl and wget
Suggests: zenity

## Intro:
tmw-info's update functions download database(DB) files to be searched locally and only require updating after a server update,
except in the case of GM Logs, where you may wish to update before searching, if looking for the newest issued commands.

All searches ignore case. Only GM log searches can use any(search) and exact(esearch) terms. Other searches use all terms only.

Because tmw-info searches the actual DB files, hidden attributes are seen in the results.

## Tips:
When searching for references of items and mobs, remember to check it's ID and search with it too. When items and mobs are spawned, ID's are
often used rather than names. Item IDs for drops are in mob DB files, but names are not. It is important to remember that the files being
searched contain IDs and are in some cases, like drops, converted into human readable text for viewing results in the terminal.

When searching for monster drops, always use the item ID, which can be found easily in an item search. Item searches do not require an ID
as it's name is searchable in the DB. Also note that the name in game may differ in rare cases, like the Magic Ring being the MageRing.

gm-show is the only GUI aspect of tmw-info at the moment, but defaults to terminal output if zenity isn't installed (I didn't
uninstall zenity to test, but echoing to terminal should work). It works the exact same as tmw-gmlog and is available separately on my
projects page (see top). It is intented to be called from a preset keyboard shortcut (I run ManaPlus in window mode and use OS KB shortcuts
to toggle fullscreen. Even when in fullscreen, the GUI window will show on top this way when the key combo is pressed). It is also important
to mention, there is a delay between the GM CMDs issued and the server's log being updated.

If trying to compile a complete DB file of all items, for instance, a common character must be found in all references because
wildcards will not work. " , " can be used to search all files since it is the common string separator in all, but will have a lot of
empty space and junk lines and even some misplaced information. Due to the IDs having different character lengths (no preceeding 0s),
sorting them in the proper order can also be a challenge. You can encounter orders like this: 5524, 56, 5576, 558.

If you still wish to create a full list: " grep -E '^[0-9]+,\s*' | sort -n | tee -a  items.txt < <(tmw-info item-search ,) "
Due to the subshell, you will have to manually quit when the output stops with ctrl-c before the file is populated.


## Help:
```
tmw-info [command] [STRING]
    help / --help / -h (this help screen)
    cmds-update (downloads command source files. Should be run before using "cmds")
    cmds (ManaPlus and GM command reference)
    news (reads game news)
    online (shows online players)
    gm-update (downloads all GM logs. Should be run before searching)
    gm-search TERM1 TERM2 TERM3 ... (results contain any of the terms)
    gm-asearch TERM1 TERM2 TERM3 ... (results contain all of the terms)
    gm-esearch TERM1 TERM2 TERM3 ... (results contain the exact term)
    gm-show (if zenity is installed, displays a GUI pop-up of the last 10 issued GM CMDs), otherwise in terminal
    chat-search TERM1 TERM2 TERM3 ... (chat: results contain all of the terms)
    item-update (downloads item DB. Must be run before searching)
    item-search TERM1 TERM2 TERM3 ... (items: results contain all of the terms)
    mob-update (downloads monster DB. Must be run before searching)
    mob-search TERM1 TERM2 TERM3 ... (monsters: results contain all of the terms)
If you get and error while searching, it may contain special characters and needs to be quoted
gm searches, item-search, mob-search and cmds require periodic updating with their respective update commands
To avoid hammering servers, there will be no update-all option. Use gm-update before performing gm searches.
Use gm-show to quickly display the last 10 gm commands run. Updates automatically. Uses zenity to display GUI, if installed)
```

## Changelog
* 2023-08-01 - Added account age by number to the online list, whilst leaving it in alphabetical order
             - Corrected server URLs to be more exact, instead of looking for patterns and added rm lines, since wget was not overwriting/updating files
* 2023-07-29 - Updated many server URLs to current
* 2023-01-22 - mob-search was showing other incorrect info, since CRIT DEF was added to mob DB. Info shows correctly now and more info is shown
* 2023-01-06 - Fixed the ordering of the online list to ignore case, so it shows players in a more organized fashion
* 2023-01-04 - Fixed mob-search do display proper drops. This was due to a server DB update which added new columns
* 2022-12-13 - Changed check for zenity from dpkg -l to find in /bin or /usr/bin
           - Added general check and warning for those without curl or wget
           - Used find for removing .tmp files instead of specifying locations in the exit trap
           - Edited structure, comments and readme
* 2022-06-30 - corrected update-cmds to cmds-update and missing data from web sources now shows
           - cmds now pulls text from the M+ binary and some commands have been added manually that were found by chance
           - Added extra help text
* 2021-04-13 - added cmds-update to download @ command DB pages and jak1's wiki on client commands
           - "cmds" now gets info from those pages and is better organized
           - Added extra function to chat-search to view a log file easily after a search
           - Fixed a bug that downloaded 2020-09 GM log everytime gm-update was run
           - Added support for GM log files ending in "-old" (2020-09-old is a restored file)
* 2020-09-16 - Added chat log search
* 2020-08-16 - Added ratio for dropped items to represent chances in a more human readable way
* 2020-08-15 - Fixed gm-show (wasn't showing GUI) and removed trailing comma in mob drops

